<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {

	public function index()
	{
		$this->load->model('Obat_model');
		$data['obat'] = $this->Obat_model->getObat("obat");
		$data['jenis'] = $this->Obat_model->getObat("jenis");
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;

		}
		$data["total_cart"] = $i;
		$data['content'] = 'index';
		$this->load->view('templates/template', $data);

	}

	public function getObat(){
		$this->load->model('Obat_model');
		$data['obat'] = $this->Obat_model->getAllObat();
		$this->load->view('admin/Obat', $data);
	}

	public function getJenis(){
		$this->load->model('Obat_model');
		$data['jenis'] = $this->Obat_model->getObat("jenis");
		$this->load->view('admin/Jenis', $data);
	}

	public function getForm(){
		$this->load->model('Obat_model');
		$data['jenis'] = $this->Obat_model->getObat("jenis");
		$this->load->view('admin/Forms_tambah_Obat', $data);
	}

	public function getCus(){
		$this->load->model('Obat_model');
		$data['cus'] = $this->Obat_model->getObat("customer");
		$this->load->view('admin/Customer', $data);
	}

	public function detailBarang($id){
		$this->load->model('Obat_model');
		$data['detail'] = $this->Obat_model->getElement($id);
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'detailBarang';
		$this->load->view('templates/template', $data);
	}

	public function addCart($id){
		$this->load->model('Obat_model');
		$getData = $this->Obat_model->getElement($id);
		$data = array('id'=>$getData->id, 
					'name'=>$getData->nama_obat,
					'qty' => 1, 
					'price'=>$getData->harga);
		$i = 0;
		foreach ($this->cart->contents() as $key) {

			if ($getData->stok_obat <= $key['qty']) {
				$this->session->set_flashdata('status_obat', '<h3 style="color: red;">Stok Obat Kurang</h3>');
				redirect('Obat/showCart');
			}
			$i++;
		}
		$this->cart->insert($data);
		$halaman['total_cart'] = $i;
		$halaman['content'] = 'Keranjang_Belanja';
		$this->load->view('templates/template', $halaman);
	}

	public function savePembelian(){
		$this->load->model('Obat_model');
		$i = 1;
		$jumlah = 0;
		foreach ($this->cart->contents() as $key) {
			# code...
			$getData = $this->Obat_model->getElement($key["id"]);
			$jumlah = $getData->stok_obat-$key['qty'];
			$data = array(
					"id_customer"=>$this->session->userdata("id_customer"),
					"id_obat"=>$key['id'],
					"tgl_pembelian"=>date('Y-m-d'),
					"quantity"=>$key['qty'],
					"total_bayar"=>$key['subtotal'],
					"status"=>"dipesan"
					);
			$status = $this->Obat_model->savePembelian($data, $jumlah);
			$i++;
		}
		$this->cart->destroy();
		$halaman['content'] = 'CheckOut';
		$this->load->view('templates/template', $halaman);
	}

	public function hapusCart($rowid){
		$this->cart->update(array('rowid' => $rowid, 'qty'=>0));
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'Keranjang_Belanja';
		$this->load->view('templates/template', $data);
	}

	public function updateCart(){
		$this->load->model('Obat_model');
		$i = 1;
		$u = 0;
		foreach ($this->cart->contents() as $obat) {
			$getData = $this->Obat_model->getElement($obat["id"]);
			if ($getData->stok_obat < $_POST[$i.'qty']) {
				$this->session->set_flashdata('status_obat', '<h3 style="color: red;">Stok Obat Kurang</h3>');
				redirect('Obat/showCart');
			}
			else{
				$this->cart->update(array('rowid'=>$obat['rowid'], 'qty'=>$_POST[$i.'qty']));
				$i++;
				$u++;
			}
		}
		$data["total_cart"] = $u;
		$data['content'] = 'Keranjang_Belanja';
		$this->load->view('templates/template', $data);
	}

	public function showCart(){
		$keranjang = $this->cart->contents();
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'Keranjang_Belanja';
		$this->load->view('templates/template', $data);
	}

	public function cari(){
		$this->load->model('Obat_model');
		$cari = $this->input->get('cari');
		$data['obat'] = $this->Obat_model->cari($cari)->result();
		$data['jenis'] = $this->Obat_model->getObat("jenis");
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'index';

		$this->load->view('templates/template', $data);
	}

	public function insert()
	{
		// ketikan source code yang ada di modul
		$data['content'] = 'insert';
		$this->load->view('templates/template', $data);
	}

	public function tambah() 
	{
		// ketikan source code yang ada di modul
		$obat['nama_obat'] = $this->input->post('nama_obat');
		$obat['id_jenis'] = $this->input->post('jenis_obat');
		$obat['stok_obat'] = $this->input->post('stok_obat');
		$obat['tgl_exp'] = $this->input->post('tgl');
		$obat['harga'] = $this->input->post('harga');
		$obat['keterangan'] = $this->input->post('keterangan');
		$config['upload_path']          = "./assets/images";
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('gambar'))
        {
                $error = array('error' => $this->upload->display_errors());

                $this->load->view('admin/Forms_tambah_Obat', $error);
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $obat['gambar'] = $this->upload->data('file_name');
	            $this->load->model('Obat_model');

				$status = $this->Obat_model->insertObat($obat);
				if ($status) {
					$this->session->set_flashdata('status', '<h3 style="color: blue;">Obat Berhasil Ditambahkan</h3>');
				}
				else{
					$this->session->set_flashdata('status', '<h3 style="color: red;">Obat Gagal Ditambahkan</h3>');
				}

				redirect('Obat/getObat');
        }
			
	}

	public function TambahJenis(){

		$jenis['nama_jenis'] = $this->input->post('nama_jenis');

		$this->load->model('Obat_model');
		$this->Obat_model->insertJenis($jenis);
		
		 redirect('Obat/getJenis');
	}

	public function eObat($id){
		$this->load->model('Obat_model');
		$data['obat'] = $this->Obat_model->getElement($id);
		$data['jenis'] = $this->Obat_model->getObat("jenis");
		$this->load->view('admin/ubahobat', $data);
	}

	public function eJenis($id_jenis){
		$this->load->model('Obat_model');
		$data['jenis'] = $this->Obat_model->getElementJenis($id_jenis);
		$this->load->view('admin/ubahjenis', $data);
	}

	public function editJenis(){
		$this->load->model('Obat_model');
		$jenis['id_jenis'] = $this->input->post('id_jenis');
		$jenis['nama_jenis'] = $this->input->post('nama_jenis');
		$status = $this->Obat_model->updateJenis($jenis);

		redirect('obat/getJenis');
	}

	public function edit(){
		$obat["id"] = $this->input->post('id');
		$obat['nama_obat'] = $this->input->post('nama_obat');
		$obat['id_jenis'] = $this->input->post('jenis_obat');
		$obat['stok_obat'] = $this->input->post('stok_obat');
		$obat['tgl_exp'] = $this->input->post('tgl');
		$obat['harga'] = $this->input->post('harga');
		$obat['keterangan'] = $this->input->post('keterangan');
		$config['upload_path']          = "./assets/images";
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('gambar'))
        {
                $error = array('error' => $this->upload->display_errors());

                $this->load->view('admin/Forms_tambah_Obat', $error);
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $obat['gambar'] = $this->upload->data('file_name');
	            $this->load->model('Obat_model');

				$status = $this->Obat_model->updateObat($obat);
				if ($status) {
					$this->session->set_flashdata('status', '<h3 style="color: blue;">Obat Berhasil Ditambahkan</h3>');
				}
				else{
					$this->session->set_flashdata('status', '<h3 style="color: red;">Obat Gagal Ditambahkan</h3>');
				}

				
        }
        redirect('Obat/getObat');
	}

	public function selesai($Obat){
		$this->_rules();
		$this->load->model('Obat_model');
		$data['id_Obat'] = $Obat;
		$data['status'] = 1;
		$status = $this->Obat_model->updateObat($data);
		if ($status) {
				$this->session->set_flashdata('status', '<h3 style="color: blue;">Obat Berhasil Diubah</h3>');
		}
			else{
				$this->session->set_flashdata('status', '<h3 style="color: red;">Obat Gagal Diubah</h3>');
		}
		redirect('Obat');
	}

	public function undo($Obat){
		$this->_rules();
		$this->load->model('Obat_model');
		$data['id_Obat'] = $Obat;
		$data['status'] = 0;
		$status = $this->Obat_model->updateObat($data);
		if ($status) {
				$this->session->set_flashdata('status', '<h3 style="color: blue;">Obat Berhasil Diubah</h3>');
		}
			else{
				$this->session->set_flashdata('status', '<h3 style="color: red;">Obat Gagal Diubah</h3>');
		}
		redirect('Obat');
	}

	public function hapus($id){
		$this->_rules();
		$this->load->model('Obat_model');
		$status = $this->Obat_model->hapusObat($id);
		if ($status) {
				$this->session->set_flashdata('status', '<h3 style="color: blue;"> Berhasil Dihapus</h3>');
		}
			else{
				$this->session->set_flashdata('status', '<h3 style="color: red;"> Gagal Dihapus</h3>');
		}
		redirect('Obat/getObat');
	}


	public function hapusJenis($id_jenis){
		$this->_rules();
		$this->load->model('Obat_model');
		$status = $this->Obat_model->hapusJenis($id_jenis);
		
		redirect('Obat/getJenis');
	}


	public function _rules()
	{
		// ketikan source code yang ada di modul
		$this->form_validation->set_rules('Obat','nama Obatlist isi','trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">','</span>');	
	}
}
