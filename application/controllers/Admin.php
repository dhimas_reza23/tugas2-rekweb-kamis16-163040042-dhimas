<?php 
/**
 * 
 */
class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Obat_Model');
		
	}
	
	public function index(){
		$this->load->view('admin/Login');
		
	}
	
	public function checkLogin(){
		
		redirect('Pesanan/index');
		
	}
	public function tambahJenis(){
		$this->load->view('admin/Forms_tambah_jenis_Obat');
	}
	public function tambahObat(){
		redirect('obat/getForm');
	}
	public function Pesanan(){
		redirect('Pesanan/index');
	}
	public function obat(){
		redirect('obat/getObat');
	}

	public function jenisObat(){
		redirect('obat/getJenis');
	}

	public function customer(){
		redirect('obat/getCus');
	}

	public function logout(){
			$this->session->sess_destroy();
			redirect('Admin');
	}

	public function aksi_login(){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$where = array(
					"username"=>$username,
					"password"=>$password);
			$this->load->model('Admin_Model');
			$cek = $this->Admin_Model->cekLogin($where)->num_rows();
			if ($cek > 0) {
				$data_session = array(
								"nama"=>$username,
								"status_admin"=>"login");
				$this->session->set_userdata($data_session);
				redirect('Pesanan/index');
			}
			else{
				redirect('pesanan');
			}
		}
}