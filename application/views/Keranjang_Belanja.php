<title>Keranjang Belanja</title>
<nav class="navbar navbar-default">
  <div class="container-fluid">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= base_url("Obat/index"); ?>">B4 Pharmacy</a>
      </div>
      <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav text-center">
            <table>
              <form action="<?= base_url("Obat/cari") ?>">
              <thead>
                <th width="90%"><input class="form-control" placeholder="Search here" name="cari"></th>
                <th><button class="btn btn-primary" style="float: right; clear: both; "><i class="fa fa-search"></i></button></th>
              </thead>
              </form>
              </table>
          </ul>

          <ul class="nav navbar-nav navbar-right">
             <li>
                    <a href="<?= base_url('Obat/showCart') ?>">
                          <i class="fa pe-7s-cart"></i>
                          <span class="notification hidden-sm hidden-xs"><?= $total_cart; ?></span>
                    </a>  
              </li>
              <li>
                <?php if ($this->session->userdata('status') == 'login'): ?>
                  <a href="#">
                      <p>Log out</p>
                  </a>
                <?php else : ?>
                  <a href="<?= base_url("Customer/login"); ?>">
                      <p>Log In</p>
                  </a>
                <?php endif ?>
              </li>
        <li class="separator hidden-lg hidden-md"></li>
          </ul>
      </div>
  </div>
</nav>
<div class="container">  
<div class="row"></div>
<h3>Daftar Keranjang</h3>
<?= $this->session->userdata('status_obat'); ?>
<br>
    <div class="col">
            
    </div>
    <div class="col">
        <?php echo form_open('Obat/updateCart'); ?>

        <table class="table table-hover table-striped" width="80%">

                <tr>
		      <th>Opsi</th>
                        <th>QTY</th>
                        <th>Nama Barang</th>
                        <th style="text-align:right">Harga</th>
                        <th style="text-align:right">Total Harga</th>
                </tr>

                <?php $i = 1; ?>

                <?php foreach ($this->cart->contents() as $items): ?>

                <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>

                <tr>
        	        <td align="center"><a href="<?= base_url('Obat/hapusCart/'. $items['rowid']) ?>">X</a></td>
                        <td><?php echo form_input(array('name' => $i.'qty', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?></td>
                        <td><?php echo $items['name']; ?>
                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                                <p> <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                                        <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
                                        <?php endforeach; ?>
                                </p>
                        `       <?php endif; ?>
                        </td>
                        <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?></td>
                        <td style="text-align:right">Rp<?php echo $this->cart->format_number($items['subtotal']); ?></td>
                </tr>

                <?php $i++; ?>

                <?php endforeach; ?>

                <tr>
                        <td colspan="3"> </td>
                        <td class="right"><strong>Total</strong></td>
                        <td class="right">Rp<?php echo $this->cart->format_number($this->cart->total()); ?></td>
                </tr>

        </table>
        <p>
        <?php echo form_submit('', 'Update',"class='btn btn-warning'"); ?>
        <a href="<?= base_url('Obat/index') ?>" class="btn btn-success">Lanjut Belanja</a>
        <a href="<?= base_url('Customer/checkOut') ?>" class="btn btn-danger">Check Out</a></p>
    </div>

     <div class="col">
            
    </div>

</div>
</div> 