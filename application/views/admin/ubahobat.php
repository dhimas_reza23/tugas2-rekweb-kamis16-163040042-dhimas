<!DOCTYPE html>
<html lang="en">
<head>
    <title>Form tambah obat</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/icons/favicon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/icons/favicon-114x114.png">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>styles/jquery-ui-1.10.4.custom.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/animate.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/all.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/main.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/style-responsive.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/zabuto_calendar.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/pace.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/styles/jquery.news-ticker.css">
</head>
<body>
 
        <!--BEGIN TOPBAR-->
        <div id="header-topbar-option-demo" class="page-header-topbar">
            <nav id="topbar" role="navigation" style="margin-bottom: 0;" data-step="3" class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <a id="logo" href="index.html" class="navbar-brand"><span class="fa fa-rocket"></span><span class="logo-text">BFourFarmacy</span><span style="display: none" class="logo-text-icon">µ</span></a></div>
            <div class="topbar-main"><a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>
                
                <form id="topbar-search" action="" method="" class="hidden-sm hidden-xs">
                    <div class="input-icon right text-white"><a href="#"><i class="fa fa-search"></i></a><input type="text" placeholder="Search here..." class="form-control text-white"/></div>
                </form>
                
                <ul class="nav navbar navbar-top-links navbar-right mbn">
                   
                    <li class="dropdown topbar-user"><a data-hover="dropdown" href="#" class="dropdown-toggle"><img src="images/avatar/48.jpg" alt="" class="img-responsive img-circle"/>&nbsp;<span class="hidden-xs">Admin</span>&nbsp;<span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-user pull-right">
                          
                            <li><a href="<?= base_url('Admin/logout'); ?>"><i class="fa fa-key"></i>Log Out</a></li>
                        </ul>
                    </li>
                   
                </ul>
            </div>
        </nav>
           
        
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;"
                data-position="right" class="navbar-default navbar-static-side">
            <div class="sidebar-collapse menu-scroll">
                <ul id="side-menu" class="nav">
                    
                     <div class="clearfix"></div>
         
                    <li><a href="<?php echo base_url('Admin/Pesanan'); ?>"><i class="fa fa-shopping-cart fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i><span class="menu-title">Pesanan</span></a></li>
                    
                    <li class="active"><a href="<?php echo base_url('index.php/Admin/tambahObat') ?>"><i class="fa fa-edit fa-fw">
                        <div class="icon-bg bg-violet"></div>
                    </i><span class="menu-title">Tambah Obat</span></a>
                      
                    </li>
                     <li><a href="<?php echo base_url('Admin/tambahJenis') ?>"><i class="fa fa-edit fa-fw">
                        <div class="icon-bg bg-violet"></div>
                    </i><span class="menu-title">Tambah Jenis Obat</span></a>
                      
                    </li>
                    <li><a href="<?php echo base_url('Admin/Obat') ?>"><i class="fa fa-th-list fa-fw">
                        <div class="icon-bg bg-blue"></div>
                    </i><span class="menu-title">Obat</span></a>
                          
                    </li>
                    <li><a href="<?php echo base_url('Admin/jenisObat') ?>"><i class="fa fa-th-list fa-fw">
                        <div class="icon-bg bg-blue"></div>
                    </i><span class="menu-title">Jenis Obat</span></a>
                          
                    </li>
                    
                    <li><a href="<?php echo base_url('Admin/akunPelanggan') ?>"><i class="fa fa-user fa-fw">
                        <div class="icon-bg bg-yellow"></div>
                    </i><span class="menu-title">Customer</span></a>
                       
                    </li>
                </ul>
            </div>
        </nav>
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            Forms</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="Pesanan">Home</a>&nbsp;&nbsp;<i
                            class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Forms</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Form Ubah obat</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                            <div class="col-lg-12">
                                <div class="col-md-12">
                                    <div id="area-chart-spline" style="width: 100%; height: 300px; display: none;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
                                    <div class="panel panel-orange">
                                            <div class="panel-heading">
                                                Tambah Obat</div>
                                            <div class="panel-body pan">
                                                <form action="<?= base_url('Obat/edit'); ?>" method="post" enctype="multipart/form-data">
                                                <input id="inputName" name="id" type="hidden" value="<?= $obat->id; ?>" /></div>
                                                <div class="form-body pal">
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-medkit"></i>
                                                            <input id="inputName" name="nama_obat" type="text" placeholder="NamaObat" class="form-control" value="<?= $obat->nama_obat; ?>" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                    
                                                        <select class="form-control" name="jenis_obat">
                                                            <option >Jenis Obat</option>
                                                            <?php foreach ($jenis as $key): ?>
                                                            <option value="<?= $key->id_jenis; ?>"><?= $key->nama_jenis; ?></option>
                                                            <?php endforeach ?>
                                                        </select></div> 
                                                    
                                                    
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-bars"></i>
                                                            <input id="inputStok" name="stok_obat" type="number" placeholder="JumlahObat" class="form-control" value="<?= $obat->stok_obat; ?>"/></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-calendar"></i>
                                                            <input id="inputDate" type="date" placeholder="tanggalkadaluarsa" class=		"form-control" name="tgl" value="<?= $obat->tgl_exp; ?>"/></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-money"></i>
                                                            <input id="inputHarga" type="number" placeholder="Harga" name="harga" class="form-control" value="<?= $obat->harga; ?>"/></div>
                                                    </div>
													<div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-upload"></i>
                                                            <input type="file" name="gambar" id="input_import_file" class="form-control" name="gambar" value="<?= $obat->gambar; ?>">
                                                            </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-pencil"></i>
                                                            <input id="inputKeterangan" name="keterangan" type="text" placeholder="Keterangan" class="form-control" value="<?= $obat->keterangan; ?>"/></div>
                                                    </div>

                                                    
                                                    </div>
                                                </div>
                                                <div class="form-actions text-right pal">
                                                    <button type="submit" class="btn btn-primary">
                                                        Submit</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--BEGIN FOOTER-->
                        <div id="footer">
                    
                        <div class="copyright">
                             <a href="http://themifycloud.com">2014 © KAdmin Responsive Multi-Purpose Template</a></div>
                        </div>
                <!--END FOOTER-->
                </div>
                <!--END CONTENT-->
               
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
   <script src="<?php echo base_url(); ?>assets/admin/script/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/bootstrap-hover-dropdown.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/html5shiv.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/respond.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.cookie.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/icheck.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/custom.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.news-ticker.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.menu.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/pace.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/holder.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/responsive-tabs.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.flot.categories.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.flot.tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.flot.fillbetween.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/zabuto_calendar.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/index.js"></script>
    <!--LOADING SCRIPTS FOR CHARTS-->
    <script src="<?php echo base_url(); ?>assets/admin/script/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/data.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/drilldown.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/exporting.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/highcharts-more.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/charts-highchart-pie.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/script/charts-highchart-more.js"></script>
    <!--CORE JAVASCRIPT-->
    <script src="<?php echo base_url(); ?>assets/admin/script/main.js"></script>
    <script>        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-145464-12', 'auto');
        ga('send', 'pageview');


</script>
</body>
</html>
