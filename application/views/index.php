<title>B4fourFarmacy</title>
<nav class="navbar navbar-default">
  <div class="container-fluid">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= base_url("Obat/index"); ?>">B4 Pharmacy</a>
      </div>
      <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav text-center">
            <table>
              <form action="<?= base_url("Obat/cari") ?>">
              <thead>
                <th width="90%"><input class="form-control" placeholder="Search here" name="cari"></th>
                <th><button class="btn btn-primary" style="float: right; clear: both; "><i class="fa fa-search"></i></button></th>
              </thead>
              </form>
              </table>
          </ul>

          <ul class="nav navbar-nav navbar-right">
             <li>
                    <a href="<?= base_url('Obat/showCart') ?>">
                          <i class="fa pe-7s-cart"></i>
                          <span class="notification hidden-sm hidden-xs"><?= $total_cart; ?></span>
                    </a>  
              </li>
              <li>
                <?php if ($this->session->userdata('status') == 'login'): ?>
                  <a href="<?= base_url("Customer/logout"); ?>">
                      <p>Log out</p>
                  </a>
                <?php else : ?>
                  <a href="<?= base_url("Customer/login"); ?>">
                      <p>Log In</p>
                  </a>
                <?php endif ?>
              </li>
        <li class="separator hidden-lg hidden-md"></li>
          </ul>
      </div>
    </div>
  </div>
</nav>
 <div class="container-fluid">
      <div class="row">
        <h3 class="title" style="font-family:arial; text-align: center;">Daftar Obat</h3>
        <?php foreach( $obat as $row ) : ?>
          <div class="col-md-3">
              <div class="card">
                  <div class="content text-center">
                      <img src="<?= base_url('assets/images/'. $row->gambar); ?>" alt="Card image cap" width="200px" height="200px" alt="">
                      <div class="footer">
                           <h5 class="title"><?= $row->nama_obat; ?></h5>
                           <hr>
                           <h5 class="title">Rp. <?= $row->harga; ?></h5>
                           <hr>
                           <h5 class="title">Stok Obat : <?= $row->stok_obat; ?></h5>
                           <hr>
                           <a href="<?= base_url('Obat/addCart/'.$row->id); ?>" class="btn btn-success"><i class="fa pe-7s-cart"></i>Buy</a>
                      </div>
                  </div>
              </div>
          </div>
          <?php endforeach; ?>
  </div>
</div>