<?php
/**
 * 
 */
class Pesanan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Pesanan_Model');
	}

	public function index(){
		$data['pesanan'] = $this->Pesanan_Model->getAll();
		$this->load->view('admin/Pesanan', $data);
	}

	public function ubahStatus($id){
		$data['id_pembelian'] = $id;
		$data['status'] = "selesai";
		$status = $this->Pesanan_Model->updateStatus($data);
		if ($status) {
				$this->session->set_flashdata('status', '<h3 style="color: blue;">Todo Berhasil Diubah</h3>');
		}
			else{
				$this->session->set_flashdata('status', '<h3 style="color: red;">Todo Gagal Diubah</h3>');
		}
		redirect('Pesanan');
	}
}
?>