<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat_model extends CI_Model {

	public function getObat($tabel){
		return $this->db->get($tabel)->result();
	}

	public function getAllObat(){
		$this->db->select("*");
		$this->db->from("obat");
		$this->db->join("jenis", "obat.id_jenis=jenis.id_jenis");
		return $this->db->get()->result();
	}
	public function getElement($id){
		return $this->db->get_where('obat', array('id' => $id))->row();
	}

	public function getElementJenis($id){
		return $this->db->get_where('jenis', array('id_jenis' => $id))->row();
	}


	// public function insertObat($Obat){
	// 	$this->db->insert('Obat', $Obat);
	// 	return $this->db->affected_rows();
	// }

	public function insertObat($obat){
		$this->db->insert('obat', $obat);
		return $this->db->affected_rows();
	}

	public function insertJenis($jenis){
		$this->db->insert('jenis', $jenis);
		return $this->db->affected_rows();
	}

	public function savePembelian($data, $jumlah){
		$this->db->insert('pembelian', $data);
		$this->db->where('id', $data['id_obat']);
		$this->db->update('obat', array('stok_obat'=>$jumlah));
		return $this->db->affected_rows();
	}

	public function updateObat($data = []){
		$id = $data['id'];
		$this->db->where('id', $id);
		$this->db->update('obat', $data);
		return $this->db->affected_rows();
	}

	public function updateJenis($data = []){
		$id_jenis = $data['id_jenis'];
		$this->db->where('id_jenis', $id_jenis);
		$this->db->update('jenis', $data);
		return $this->db->affected_rows();
	}
	// public function updateObat($data = []){
	// 	$id = $data['id_Obat'];
	// 	$status = $data['status'];
	// 	$this->db->where('id_Obat', $id);
	// 	$this->db->update('Obat', array('status'=>$status));
	// 	return $this->db->affected_rows();
	// }

	public function deleteObat($id){
		$this->db->where('id_Obat', $id);
		$this->db->delete('Obat');
		return $this->db->affected_rows();
	}

	public function cari($cari){
		$this->db->select('*');
		$this->db->from('obat');
		$this->db->join('jenis', 'jenis.id_jenis = obat.id_jenis');
		$this->db->like('obat.nama_obat', $cari);
		$this->db->or_like('jenis.nama_jenis', $cari);
		$this->db->or_like('obat.harga', $cari);
		return $this->db->get();
	}

	public function hapusObat($id){
		$this->db->where('id',$id);
		$this->db->delete('obat');
		return $this->db->affected_rows();
	}

	public function hapusJenis($id_jenis){
		$this->db->where('id_jenis',$id_jenis);
		$this->db->delete('jenis');
		return $this->db->affected_rows();
	}
}
?>
