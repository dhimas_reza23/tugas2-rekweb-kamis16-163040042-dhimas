<?php
/**
 * 
 */
class Customer extends CI_Controller
{
	public function index(){
		$data['content'] = 'Login';
		$this->load->view('templates/template', $data);
	}

	public function checkOut(){
		if ($this->session->userdata('status') != 'login') {
			redirect('Customer/login');
		}
		else{
			redirect('Obat/savePembelian');
		}
	}

	public function login(){
		$data['content'] = 'Login';
		$this->load->view('templates/template', $data);
	}

	public function registerView(){
		$data['content'] = 'Register';
		$this->load->view('templates/template', $data);
	}

	public function simpanCus(){
		$data = array(
				"nama"=>$this->input->post('nama'),
				"alamat"=>$this->input->post('alamat'),
				"email"=>$this->input->post('email'),
				"password"=>$this->input->post('password'),
				"jenis_kelamin"=>$this->input->post('jk'),
				"tgl_lahir"=>$this->input->post('tgl_lahir'),
				"no_telp"=>$this->input->post('notel'));

		$this->load->model('Customer_model');

			$status = $this->Customer_model->insertCustomer($data);
			if ($status) {
				$this->session->set_flashdata('status', '<h3 style="color: blue;">Customer Berhasil Ditambahkan</h3>');
			}
			else{
				$this->session->set_flashdata('status', '<h3 style="color: red;">Customer Gagal Ditambahkan</h3>');
			}

			redirect('Customer/login');
		}

		public function aksi_login(){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$where = array(
					"nama"=>$username,
					"email"=>$username,
					"password"=>$password);
			$this->load->model('Customer_Model');
			$cek = $this->Customer_Model->cekLogin($where)->num_rows();
			$id_customer = $this->Customer_Model->getId($username);
			if ($cek > 0) {
				$data_session = array(
								"nama"=>$username,
								"status"=>"login",
								"id_customer"=>$id_customer->id_customer);
				$this->session->set_userdata($data_session);
				redirect('Obat');
			}
			else{
				redirect('customer');
			}
		}

		public function logout(){
			$this->session->sess_destroy();
			redirect('Obat');
		}	
}
?>