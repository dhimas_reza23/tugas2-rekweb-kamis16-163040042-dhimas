<?php
	/**
	 * 
	 */
	class Pesanan_Model extends CI_Model
	{
		
		public function getAll(){
			$this->db->select("*");
			$this->db->from("pembelian");
			$this->db->join("customer", "pembelian.id_customer = customer.id_customer");
			$this->db->join("obat", "obat.id=pembelian.id_obat");
			return $this->db->get()->result();
		}

		public function updateStatus($data = []){
			$id = $data['id_pembelian'];
			$status = $data['status'];
			$this->db->where('id_pembelian', $id);
			$this->db->update('pembelian', array('status'=>$status));
			return $this->db->affected_rows();
		}
	}
?>